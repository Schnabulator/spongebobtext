package main

import (
	"bytes"
	"fmt"
	"os"
	"strings"
)

func main() {
	// First just read the parameter
	args := os.Args
	text := strings.Join(os.Args[1:], " ")
	if len(args) > 1 {
		fmt.Printf("Original: %v\nReplaced: %v\n", text, sponge(args[1:]))
	}
}

func sponge(original []string) string {
	replaced := ""
	for _, s := range original {
		text := []byte(s)
		nextUpper := false
		for i, e := range text {
			// backup := e
			bs := []byte{e}
			if i == 0 {
				// Just determine what should happen with the next character
				nextUpper = isUpper(e)
				fmt.Printf("2nd letter %v\n", nextUpper)
				replaced += string(bs)
				continue
			}
			if nextUpper {
				re := bytes.ToUpper(bs)
				fmt.Printf("\tChanged %v to Upper\n", i)
				replaced += string(re)
			} else {
				re := bytes.ToLower(bs)
				fmt.Printf("\tChanged %v to lower\n", i)
				replaced += string(re)
			}

			nextUpper = !nextUpper
		} //Over every single string
		replaced += " "
	} //Over the string array
	return replaced
}

func isUpper(b byte) bool {
	ret := false
	if b >= 65 && b <= 90 {
		ret = true
	}
	ret = false
	// fmt.Printf("received %v [%v]\n", b, ret)
	return ret
	// back := b
	// bs := []byte{b}
	// fmt.Printf("  Going to compare [%v] [%v], btw its %v\n", bs[0], b, bs[0] == b)
	// upper := bytes.ToUpper(bs)[0]
	// fmt.Printf("   Did something crazy ->%v and %v\n", upper, b)
	// if upper == back {
	// 	return true
	// }
	// return false
}
